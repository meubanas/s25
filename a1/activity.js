db.fruits.insertMany([
        {
                "name": "Banana",
                "supplier": "Farmer Fruits Inc.",
                "stocks": 30,
                "price": 20,
                "onSale": true
        },
        {
                "name": "Mango",
                "supplier": "Mango Magic Inc.",
                "stocks": 50,
                "price": 70,
                "onSale": true
        },
        {
                "name": "Dragon Fruit",
                "supplier": "Farmer Fruits Inc.",
                "stocks": 10,
                "price": 60,
                "onSale": true
        },
        {
                "name": "Grapes",
                "supplier": "Fruity Co.",
                "stocks": 30,
                "price": 100,
                "onSale": true
        },
        {
                "name": "Apple",
                "supplier": "Apple Valley",
                "stocks": 0,
                "price": 20,
                "onSale": false
        },
        {
                "name": "Papaya",
                "supplier": "Fruity Co.",
                "stocks": 15,
                "price": 60,
                "onSale": true
        },
])

// count the total number of fruits onsale.

db.fruits.aggregate([
                {$match: {"onSale": true}},
                {$sort: {"name": -1}}
        ])

// count the total number of total number stock more than 20

db.fruits.aggregate([
        {$match: {stocks: 20}},
        {$group: {_id: "$name", total: {$sum: "$amount"}}}
]);

// average operator for supplier

db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: "$price", enoughStock: {$avg: "$amount"}}}
]);

// max operator for supplier price

db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: "$price", enoughStock: {$max: "$amount"}}}
]);

// min operator for supplier price

db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: "$price", enoughStock: {$min: "$amount"}}}
]);
